XORApp.controller('CongruenceController', function ($scope) {

    $scope.Home = {
        Title: "Congruencia",
    }

    $scope.number = "0";
    $scope.esPrimo = "";
    $scope.modUno = 0;
    $scope.modDos = 0;
    $scope.modTres = 0;
    $scope.result = "";
    $scope.primo = false;



    $scope.Ejecutar = function () {
        if (isNaN($scope.number)) {
            window.alert("Debe ingresar un valor numérico!");
        } else {
            if ($scope.number < 5) {
                window.alert("El número debe ser mayor a 5!");
            }
            else {
                if (primo($scope.number)) {
                    $scope.modUno = modulo($scope.number, 6);
                    $scope.modDos = modulo(1, 6);
                    $scope.modTres = modulo(5, 6);
                    $scope.result = ""
                };

            }
        }

    }

    //Primo
    function primo(num) {
        $scope.primo = false;
        $scope.esPrimo = "El número " + num + " NO es primo";
        if (num == 2) {
            $scope.esPrimo = "El número " + num + " es primo!!";
            $scope.primo = true;
        }
        else {
            for (var i = 1; i <= num; i++) {
                if (i != 1 && i != num) {
                    var result = modulo(num, i);
                    if (result == 0) {
                        $scope.esPrimo = "El número " + num + " NO es primo";
                        $scope.primo = false;
                        return false;
                    } else {
                        $scope.esPrimo = "El número " + num + " es primo!!";
                        $scope.primo = true;
                    }
                }
            }
        }
        return $scope.primo;
    }

    //Modulo
    function modulo(a, b) {
        var cociente = parseInt(a / b);
        cociente = cociente * b;
        return a - cociente;
    }





});

