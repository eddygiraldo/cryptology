XORApp.controller('ExercisesController', function ($scope, $timeout) {

    $scope.Home = {
        Title: "Desencriptar mensaje",
    }

    $scope.Message = "“53â€¡â€¡â€ 305))6*;4826)4â€¡)4â€¡);806*;48â€ 8Â¶60))85;1â€¡);:â€¡*8â€ 83(88)5*â€ ;46(;88*96*?;8)*â€¡(;485);5*â€2:*â€¡(;4956*2(5*â€”4)8Â¶8*4069285);)6â€8)4â€¡â€¡;1(â€¡9;48081;8:8â€¡1;48â€85;4)485â€528806*81(â€¡9;48;(88;4(â€¡?34;48)4â€¡;161;:188;â€¡?;”";
    $scope.Characters = [];
    $scope.Words = [];
    $scope.Replaced = [];
    $scope.IndicesOfThe = [];
    var i = 0;

    function mostLetterRepetead() {

        for (var i = 0; i < $scope.Message.length; i++) {
            var key = $scope.Message[i];

            if ($scope.Characters.filter(e => e.id == key).length > 0) {
                var index = $scope.Characters.findIndex((e => e.id == key));
                $scope.Characters[index].value = $scope.Characters[index].value + 1;
            } else {
                $scope.Characters.push({ id: key, value: 1, used: false });
            }
        }

        $scope.Characters.sort(function(a, b) {
            return parseFloat(b.value) - parseFloat(a.value);
        });
    }

    function mostWordOfTheRepetead() {

        for (var i = 0; i < $scope.IndicesOfThe.length; i++) {
            var key = $scope.Message[$scope.IndicesOfThe[i]-2] + $scope.Message[$scope.IndicesOfThe[i]-1] + $scope.Message[$scope.IndicesOfThe[i]];

            if ($scope.Words.filter(e => e.id == key).length > 0) {
                var index = $scope.Words.findIndex((e => e.id == key));
                $scope.Words[index].value = $scope.Words[index].value + 1;
            } else {
                $scope.Words.push({ id: key, value: 1, used: false });
            }
        }

        $scope.Words.sort(function(a, b) {
            return parseFloat(b.value) - parseFloat(a.value);
        });
    }

    mostLetterRepetead();

    function replaceCharacters() {

        //letter most repeated is "e"
        $scope.Replaced[0] = $scope.Message.replace(RegExp($scope.Characters[0].id,"g"), "e");

        //word most repeated is "the"
        for(var i=0; i<$scope.Message.length;i++) {
            if ($scope.Message[i] === $scope.Characters[0].id) $scope.IndicesOfThe.push(i);
        }

        mostWordOfTheRepetead();

        $scope.Replaced[1] = $scope.Replaced[0].replace(RegExp($scope.Words[0].id[0],"g"), "t");
        $scope.Replaced[1] = $scope.Replaced[1].replace(RegExp($scope.Words[0].id[1],"g"), "h");

        $scope.Replaced[2] = $scope.Replaced[1].replace(RegExp("[" + $scope.Characters[10].id + "]","g"), "r");

        $scope.Replaced[3] = $scope.Replaced[2].replace(RegExp($scope.Characters[15].id,"g"), "g");
        $scope.Replaced[3] = $scope.Replaced[3].replace(RegExp("[" + $scope.Characters[18].id + "]","g"), "u");
        $scope.Replaced[3] = $scope.Replaced[3].replace(RegExp($scope.Characters[1].id + $scope.Characters[2].id + $scope.Characters[6].id,"g"), "o");

        $scope.Replaced[4] = $scope.Replaced[3].replace(RegExp("[" + $scope.Characters[5].id + "]","g"), "s");

        $scope.Replaced[5] = $scope.Replaced[4].replace(RegExp($scope.Characters[1].id + $scope.Characters[2].id + $scope.Characters[16].id,"g"), "d");

        $scope.Replaced[6] = $scope.Replaced[5].replace(RegExp($scope.Characters[9].id ,"g"), "i");
        $scope.Replaced[6] = $scope.Replaced[6].replace(RegExp("[" + $scope.Characters[7].id + "]" ,"g"), "n");
        $scope.Replaced[6] = $scope.Replaced[6].replace(RegExp("[" + $scope.Characters[14].id + "]" ,"g"), "m");

        $scope.Replaced[7] = $scope.Replaced[6].replace(RegExp("[" + $scope.Characters[8].id + "]" ,"g"), "a");
        $scope.Replaced[7] = $scope.Replaced[7].replace(RegExp("[" + $scope.Characters[12].id + "]" ,"g"), "l");

        $scope.Replaced[8] = $scope.Replaced[7].replace(RegExp("[" + $scope.Characters[11].id + "]" ,"g"), "f");

        $scope.Replaced[9] = $scope.Replaced[8].replace(RegExp("[" + $scope.Characters[13].id + "]" ,"g"), "b");

        $scope.Replaced[10] = $scope.Replaced[9].replace(RegExp($scope.Characters[19].id + $scope.Characters[20].id  ,"g"), "v");

        $scope.Replaced[11] = $scope.Replaced[10].replace(RegExp("[" + $scope.Characters[17].id + "]" ,"g"), "y");
    }

    replaceCharacters();




});

