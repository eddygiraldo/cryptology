var XORApp = angular.module('XORApp', ['ngRoute']);

XORApp.controller('MainController', function($scope, $route, $routeParams, $location) {
        $scope.$route = $route;
        $scope.$location = $location;
        $scope.$routeParams = $routeParams;
    })

    XORApp.config(function($routeProvider, $locationProvider) {
    $routeProvider
    .when('/', {
        templateUrl: '../View/home.html',
        controller: 'HomeController'
    })
    .when('/ExerciseOne', {
        templateUrl: '../View/Exercises/exerciseOne.html',
        controller: 'ExercisesController'
    })  
    .when('/Congruence', {
        templateUrl: '../View/Exercises/congruence.html',
        controller: 'CongruenceController'
    })  
    .otherwise({
        redirectTo: '/'
    });

    //$locationProvider.html5Mode(true);
});